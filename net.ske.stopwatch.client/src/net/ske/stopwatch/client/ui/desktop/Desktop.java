package net.ske.stopwatch.client.ui.desktop;

import net.ske.stopwatch.client.ui.forms.DesktopForm;
import net.ske.stopwatch.shared.Icons;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.logger.IScoutLogger;
import org.eclipse.scout.commons.logger.ScoutLogManager;
import org.eclipse.scout.rt.client.ui.action.keystroke.AbstractKeyStroke;
import org.eclipse.scout.rt.client.ui.desktop.IDesktop;
import org.eclipse.scout.rt.extension.client.ui.desktop.AbstractExtensibleDesktop;
import org.eclipse.scout.rt.shared.TEXTS;

public class Desktop extends AbstractExtensibleDesktop implements IDesktop {
  private static IScoutLogger logger = ScoutLogManager.getLogger(Desktop.class);

  DesktopForm m_desktopForm;

  public DesktopForm getDesktopForm() {
    return m_desktopForm;
  }

  public Desktop() {
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("ApplicationTitle");
  }

  @Override
  protected boolean getConfiguredTrayVisible() {
    return true;
  }

  @Override
  protected void execOpened() throws ProcessingException {
    m_desktopForm = new DesktopForm();
    m_desktopForm.setIconId(Icons.EclipseScout);
    m_desktopForm.startView();
  }

  @Order(10.0)
  public class ResetStopwatchKeyStroke extends AbstractKeyStroke {

    @Override
    protected String getConfiguredKeyStroke() {
      return "escape";
    }

    @Override
    protected void execAction() throws ProcessingException {
      getDesktopForm().resetTimer();
    }
  }

  @Order(20.0)
  public class TimerControlKeyStroke extends AbstractKeyStroke {

    @Override
    protected String getConfiguredKeyStroke() {
      return "space";
    }

    @Override
    protected boolean getConfiguredToggleAction() {
      return true;
    }

    @Override
    protected void execToggleAction(boolean running) throws ProcessingException {
      if (running) {
        getDesktopForm().startTimer();
      }
      else {
        getDesktopForm().pauseTimer();
      }
    }
  }
}
