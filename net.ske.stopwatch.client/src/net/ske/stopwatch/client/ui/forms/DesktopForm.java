package net.ske.stopwatch.client.ui.forms;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.Date;

import net.ske.stopwatch.client.ClientSession;
import net.ske.stopwatch.client.ui.desktop.Desktop;
import net.ske.stopwatch.client.ui.formfields.AbstractStopwatchDisplayField;
import net.ske.stopwatch.client.ui.forms.DesktopForm.MainBox.ButtonBox.CopyPasteButton;
import net.ske.stopwatch.client.ui.forms.DesktopForm.MainBox.TimeDisplayBox;
import net.ske.stopwatch.client.ui.forms.DesktopForm.MainBox.TimeDisplayBox.HourField;
import net.ske.stopwatch.client.ui.forms.DesktopForm.MainBox.TimeDisplayBox.MinuteField;
import net.ske.stopwatch.client.ui.forms.DesktopForm.MainBox.TimeDisplayBox.SecondField;
import net.ske.stopwatch.shared.Icons;
import net.ske.stopwatch.shared.codetypes.TimeFormatCodeType;
import net.ske.stopwatch.shared.codetypes.TimeFormatCodeType.AbstractTimeFormatCode;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.logger.IScoutLogger;
import org.eclipse.scout.commons.logger.ScoutLogManager;
import org.eclipse.scout.rt.client.ClientAsyncJob;
import org.eclipse.scout.rt.client.ClientSyncJob;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;

public class DesktopForm extends AbstractForm {
  private static IScoutLogger logger = ScoutLogManager.getLogger(Desktop.class);

  private Date m_startDate;
  private Date m_displayDate;
  private ClientAsyncJob m_timerJob;
  private Date m_pauseDate;

  public DesktopForm() throws ProcessingException {
    super();
  }

  private String formatElapsedTime(AbstractTimeFormatCode timeFormat) {
    if (getDisplayDate() == null || getStartDate() == null) {
      return "0";
    }
    if (timeFormat == null) {
      return new Date(getDisplayDate().getTime() - getStartDate().getTime()).toString();
    }
    return timeFormat.format(new Date(getDisplayDate().getTime() - getStartDate().getTime()));
  }

  private void initTimerJob() {
    setTimerJob(new ClientAsyncJob("stopwatchJob", ClientSession.get()) {
      @Override
      protected IStatus runStatus(IProgressMonitor monitor) {
        if (getStartDate() == null) {
          setStartDate(new Date());
        }
        setDisplayDate(new Date());
        long elapsedTime = getDisplayDate().getTime() - getStartDate().getTime();
        final int seconds = (int) (elapsedTime / 1000) % 60;
        final int minutes = (int) ((elapsedTime / (1000 * 60)) % 60);
        final int hours = (int) ((elapsedTime / (1000 * 60 * 60)) % 24);

        (new ClientSyncJob("stopwatchTimeUpdateJob", ClientSession.get()) {
          @Override
          protected void runVoid(@SuppressWarnings("hiding") IProgressMonitor monitor) throws Throwable {
            getHourField().setValue(hours);
            getMinuteField().setValue(minutes);
            getSecondField().setValue(seconds);
          }
        }).schedule();

        this.schedule(1000);

        return Status.OK_STATUS;
      }
    });
  }

  @Override
  protected boolean getConfiguredAskIfNeedSave() {
    return false;
  }

  @Override
  protected int getConfiguredDisplayHint() {
    return DISPLAY_HINT_VIEW;
  }

  @Override
  protected String getConfiguredDisplayViewId() {
    return VIEW_ID_CENTER;
  }

  @Override
  protected String getConfiguredIconId() {
    return Icons.EclipseScout;
  }

  public void startView() throws ProcessingException {
    startInternal(new ViewHandler());
  }

  /**
   * @return the HourField
   */
  public HourField getHourField() {
    return getFieldByClass(HourField.class);
  }

  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the MinuteField
   */
  public MinuteField getMinuteField() {
    return getFieldByClass(MinuteField.class);
  }

  public SecondField getSecondField() {
    return getFieldByClass(SecondField.class);
  }

  /**
   * @return the CopypasteButton
   */
  public CopyPasteButton getCopypasteButton() {
    return getFieldByClass(CopyPasteButton.class);
  }

  public TimeDisplayBox getTimeDisplayBox() {
    return getFieldByClass(TimeDisplayBox.class);
  }

  @Order(10.0)
  public class MainBox extends AbstractGroupBox {

    @Override
    protected boolean getConfiguredGridUseUiHeight() {
      return false;
    }

    @Override
    protected double getConfiguredGridWeightX() {
      return 1.0;
    }

    @Override
    protected double getConfiguredGridWeightY() {
      return 1.0;
    }

    @Override
    protected int getConfiguredHeightInPixel() {
      return 150;
    }

    @Override
    protected int getConfiguredWidthInPixel() {
      return 300;
    }

    @Order(10.0)
    public class TimeDisplayBox extends AbstractGroupBox {

      @Override
      protected int getConfiguredGridColumnCount() {
        return 3;
      }

      @Order(10.0)
      public class HourField extends AbstractStopwatchDisplayField {

      }

      @Order(20.0)
      public class MinuteField extends AbstractStopwatchDisplayField {

        @Override
        protected int getConfiguredMaxIntegerDigits() {
          return 10;
        }

        @Override
        protected Integer getConfiguredMaxValue() {
          return 59;
        }

      }

      @Order(30.0)
      public class SecondField extends AbstractStopwatchDisplayField {

        @Override
        protected Integer getConfiguredMaxValue() {
          return 59;
        }

      }
    }

    @Order(20.0)
    public class ButtonBox extends AbstractGroupBox {
      @Override
      protected int getConfiguredGridColumnCount() {
        return 3;
      }

      @Order(10.0)
      public class TimeFormatSmartField extends AbstractSmartField<Long> {

        @Override
        protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
          return TimeFormatCodeType.class;
        }

        @Override
        protected int getConfiguredGridW() {
          return 2;
        }

        @Override
        protected boolean getConfiguredLabelVisible() {
          return false;
        }

      }

      @Order(20.0)
      public class CopyPasteButton extends AbstractButton {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("CopyPasteTime");
        }

        @Override
        protected int getConfiguredWidthInPixel() {
          return 100;
        }

        @Override
        protected void execClickAction() throws ProcessingException {
          Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
          StringSelection time = new StringSelection(formatElapsedTime(ClientSession.get().getTimeFormatCode()));
          clipboard.setContents(time, null);
        }

      }
    }

  }

  public class ViewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {

    }
  }

  public Date getDisplayDate() {
    return m_displayDate;
  }

  public void setDisplayDate(Date displayDate) {
    m_displayDate = displayDate;
  }

  public Date getStartDate() {
    return m_startDate;
  }

  public void setStartDate(Date startDate) {
    m_startDate = startDate;
  }

  @Override
  protected void execInitForm() throws ProcessingException {
    initTimerJob();
  }

  /**
   * @return the TimerJob
   */
  @FormData
  public ClientAsyncJob getTimerJob() {
    return m_timerJob;
  }

  /**
   * @param timerJob
   *          the TimerJob to set
   */
  @FormData
  public void setTimerJob(ClientAsyncJob timerJob) {
    m_timerJob = timerJob;
  }

  public void resetTimer() {
    getTimerJob().cancel();
    getHourField().setValue(0);
    getSecondField().setValue(0);
    getMinuteField().setValue(0);
  }

  public void startTimer() {
    if (getPauseDate() != null) {
      setStartDate(new Date(getStartDate().getTime() - (getPauseDate().getTime() - new Date().getTime())));
    }
    getTimerJob().schedule();
  }

  public void pauseTimer() {
    getTimerJob().cancel();
    setPauseDate(new Date());
  }

  public Date getPauseDate() {
    return m_pauseDate;
  }

  public void setPauseDate(Date pauseDate) {
    m_pauseDate = pauseDate;
  }
}
