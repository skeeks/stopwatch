package net.ske.stopwatch.client.ui.formfields;

import java.text.DecimalFormat;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.form.fields.integerfield.AbstractIntegerField;

public abstract class AbstractStopwatchDisplayField extends AbstractIntegerField {

  @Override
  protected void initConfig() {
    super.initConfig();
    this.setFormat(new DecimalFormat("00"));
  }

  @Override
  protected void execInitField() throws ProcessingException {
    this.setValue(0);
  }

  @Override
  protected int getConfiguredMaxIntegerDigits() {
    return 2;
  }

  @Override
  protected Integer getConfiguredMinValue() {
    return 0;
  }

  @Override
  protected String getConfiguredFont() {
    return "BOLD-30-Arial";
  }

  @Override
  protected boolean getConfiguredFocusable() {
    return false;
  }

  @Override
  protected boolean getConfiguredGridUseUiHeight() {
    return true;
  }

  @Override
  protected int getConfiguredWidthInPixel() {
    return 25;
  }

  @Override
  protected String execFormatValue(Integer validValue) {
    return getFormat().format(validValue);
  }

  @Override
  protected boolean getConfiguredLabelVisible() {
    return false;
  }

  @Override
  protected boolean getConfiguredAutoAddDefaultMenus() {
    return false;
  }

  @Override
  protected boolean getConfiguredEnabled() {
    return false;
  }
}
