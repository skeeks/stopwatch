package net.ske.stopwatch.client;

import net.ske.stopwatch.client.ui.desktop.Desktop;
import net.ske.stopwatch.shared.codetypes.TimeFormatCodeType.AbstractTimeFormatCode;
import net.ske.stopwatch.shared.codetypes.TimeFormatCodeType.HourFormatCode;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.logger.IScoutLogger;
import org.eclipse.scout.commons.logger.ScoutLogManager;
import org.eclipse.scout.rt.client.AbstractClientSession;
import org.eclipse.scout.rt.client.ClientJob;
import org.eclipse.scout.rt.shared.services.common.code.CODES;

public class ClientSession extends AbstractClientSession {
  private static IScoutLogger logger = ScoutLogManager.getLogger(ClientSession.class);

  private AbstractTimeFormatCode m_timeFormatCode;

  public ClientSession() {
    super(true);
  }

  public static ClientSession get() {
    return ClientJob.getCurrentSession(ClientSession.class);
  }

  @Override
  public void execLoadSession() throws ProcessingException {
    setDesktop(new Desktop());
    CODES.getAllCodeTypes(net.ske.stopwatch.shared.Activator.PLUGIN_ID);
    setTimeFormatCode(new HourFormatCode());
  }

  public AbstractTimeFormatCode getTimeFormatCode() {
    return m_timeFormatCode;
  }

  public void setTimeFormatCode(AbstractTimeFormatCode timeFormat) {
    m_timeFormatCode = timeFormat;
  }
}
