/**
 *
 */
package net.ske.stopwatch.shared.codetypes;

import java.util.Date;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

/**
 * @author ske
 */
public class TimeFormatCodeType extends AbstractCodeType<Long, Long> {

  private static final long serialVersionUID = 1L;
  /**
   *
   */
  public static final Long ID = 1000L;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public TimeFormatCodeType() throws ProcessingException {
    super();
  }

  @Override
  public Long getId() {
    return ID;
  }

  public static abstract class AbstractTimeFormatCode extends AbstractCode<Long> {
    private static final long serialVersionUID = 1L;

    public abstract String format(Date date);

  }

  @Order(10.0)
  public static class HourFormatCode extends AbstractTimeFormatCode {

    private static final long serialVersionUID = 1L;

    public static final Long ID = 1001L;

    @Override
    public Long getId() {
      return ID;
    }

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("HourFormat");
    }

    @Override
    public String format(Date date) {
      return String.valueOf((double) date.getTime() / ((double) (1000 * 60 * 60)));
    }
  }
}
